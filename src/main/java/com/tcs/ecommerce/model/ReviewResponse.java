package com.tcs.ecommerce.model;

import java.util.Date;

import lombok.Data;

@Data
public class ReviewResponse {
 private Integer rating;
 private String comment;
 private String heading;
 private Date date;
public Integer getRating() {
	return rating;
}
public void setRating(Integer rating) {
	this.rating = rating;
}
public String getComment() {
	return comment;
}
public void setComment(String comment) {
	this.comment = comment;
}
public String getHeading() {
	return heading;
}
public void setHeading(String heading) {
	this.heading = heading;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
	
}
