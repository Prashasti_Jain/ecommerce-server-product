package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class WishListRequest {

	private Integer productId;
	private String username;
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
