package com.tcs.ecommerce.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.ecommerce.entity.Order;
import com.tcs.ecommerce.service.OrderService;

@CrossOrigin(origins = "http://13.233.184.83")
@RestController
@Transactional

public class OrderController {
	@Autowired
	private OrderService service;

	@RequestMapping(method = RequestMethod.GET, value = "/orders")
	public ResponseEntity<List<Order>> findUserOrders(@AuthenticationPrincipal String username) throws Exception {

		return new ResponseEntity<>(service.getOrders(username), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/allOrders")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<Order>> findAllOrders(@AuthenticationPrincipal String username) throws Exception {

		return new ResponseEntity<>(service.getAllOrders(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveorders")
	public ResponseEntity<?> saveOrders(@RequestBody List<Order> orders, @AuthenticationPrincipal String username)
			throws Exception {
		return new ResponseEntity<>(service.saveOrders(orders, username), HttpStatus.OK);
	}

}