package com.tcs.ecommerce.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.ecommerce.entity.Brand;
import com.tcs.ecommerce.entity.Category;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.model.ProductRequest;
import com.tcs.ecommerce.model.ProductResponse;
import com.tcs.ecommerce.model.ReviewRequest;
import com.tcs.ecommerce.model.ReviewResponse;
import com.tcs.ecommerce.service.ProductReviewService;
import com.tcs.ecommerce.service.ProductService;

@CrossOrigin(origins = "http://13.233.184.83")
@RestController
@Transactional

public class ProductController {
	@Autowired
	private ProductService service;

	@Autowired
	private ProductReviewService reviewservice;

	@PostMapping("/addProduct")
	@PreAuthorize("hasRole('VENDOR')")
	public ResponseEntity<ProductResponse> addProduct(@RequestBody ProductRequest product1,
			@AuthenticationPrincipal String username) {
		ResponseEntity<ProductResponse> response = null;
		ProductResponse productObj = service.saveProduct(product1, username);
		response = new ResponseEntity<>(productObj, HttpStatus.CREATED);
		return response;
	}

	@GetMapping("/productsApproved")
	public ResponseEntity<List<ProductResponse>> findApprovedProducts() throws Exception {
		return new ResponseEntity<>(service.getProductsByProductStatus("Approved"), HttpStatus.OK);
	}

	@GetMapping("/allProductCreatedBy")
	public List<ProductResponse> findAllProductsByCreatedBy(@AuthenticationPrincipal String username) {
		return service.getProductsByCreatedBy(username);
	}

	@PostMapping("/deleteImage")
	@PreAuthorize("hasRole('VENDOR')")
	public ResponseEntity<?> deleteImage(@RequestBody int[] deleteList) {

		return new ResponseEntity<>(service.deleteImage(deleteList), HttpStatus.OK);
	}

	@PutMapping("/updateProductStatus")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> updateProductStatus(@RequestBody int[] idStat) {
		return new ResponseEntity<>(service.updateProductStatus(idStat[0], idStat[1]), HttpStatus.OK);
	}

	@PutMapping("/updateProduct/{id}")
	@PreAuthorize("hasRole('VENDOR')")
	public ResponseEntity<String> updateProduct(@RequestBody ProductRequest prodReq, @PathVariable int id) {
		return new ResponseEntity<>(service.updateProduct(prodReq, id), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/products")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<ProductResponse>> findAllProducts() throws Exception {
		return new ResponseEntity<>(service.getProducts(), HttpStatus.OK);
	}

	@PostMapping("/addProducts")
	@PreAuthorize("hasRole('VENDOR')")
	public List<Product> addProducts(@RequestBody List<Product> products, @AuthenticationPrincipal String username) {

		return service.saveProducts(products);
	}

	@PostMapping("/allProductsById")
	public ResponseEntity<List<ProductResponse>> findAllProductsById(@RequestBody int[] pIds,
			@AuthenticationPrincipal String username) {

		return new ResponseEntity<>(service.getAllProductsById(pIds), HttpStatus.OK);
	}

	@GetMapping("/allProduct/{name}")
	public List<ProductResponse> findAllProductsByName(@PathVariable String name) {
		return service.getProductsByName(name);
	}

	@GetMapping("/allProductProductStatus/{productStatus}")
	public List<ProductResponse> findAllProductsByProductStatus(@PathVariable String productStatus) {
		return service.getProductsByProductStatus(productStatus);
	}

	@GetMapping("/productById/{id}")
	public ResponseEntity<ProductResponse> findProductById(@PathVariable Integer id) {
		return new ResponseEntity<>(service.getProductById(id), HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	@PreAuthorize("hasRole('VENDOR')")
	public ResponseEntity<String> deleteProduct(@PathVariable int id, @AuthenticationPrincipal String username) {
		String dl = service.deleteProduct(id, username);
		String dt = "{\"Value\":\"" + dl + "\"}";
		return new ResponseEntity<>(dt, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addReview", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ReviewResponse> addReview(@RequestBody ReviewRequest request,
			@AuthenticationPrincipal String username) throws Exception {
		return new ResponseEntity<>(reviewservice.addProductReview(request, username), HttpStatus.CREATED);
	}

	@GetMapping("/getAlreadyRated/{pid}")
	public ResponseEntity<?> getAlreadyRatedProd(@PathVariable int pid, @AuthenticationPrincipal String username)
			throws Exception {
		return new ResponseEntity<>(reviewservice.getAlreadyRated(pid, username), HttpStatus.OK);
	}

	@GetMapping("/getReviewByPid/{pid}")
	public ResponseEntity<?> getReviewByProductId(@PathVariable int pid) throws Exception {
		return new ResponseEntity<>(reviewservice.findAllbyProductId(pid), HttpStatus.OK);
	}

	@GetMapping("/getReviewByUsername")
	public ResponseEntity<?> getReviewByUser(@AuthenticationPrincipal String username) throws Exception {
		return new ResponseEntity<>(reviewservice.findAllbyUser(username), HttpStatus.OK);
	}

	@DeleteMapping("/removeReview/{pid}")
	public String removeReview(@PathVariable Integer pid, @AuthenticationPrincipal String username) {

		String abc = reviewservice.removeReview(pid, username);
		return abc;
	}

	@GetMapping("/getAvgRating/{pid}")
	public ResponseEntity<?> getAverageRating(@PathVariable int pid) {
		return new ResponseEntity<>(reviewservice.getAverageRating(pid), HttpStatus.OK);
	}

	@PostMapping("/addCategory")
	public Category addCategory(@RequestBody Category category) {

		return service.addCategory(category.getCategoryName());
	}

	@GetMapping("/getCategory")

	public List<Category> getAllCategory() {
		return service.getCategories();
	}

	@PostMapping("/addBrand")
	public Brand addBrand(@RequestBody Brand brand) {

		return service.addBrand(brand.getBrandName());
	}

	@GetMapping("/getBrand")
	public List<Brand> getAllBrand() {
		return service.getBrands();
	}

}