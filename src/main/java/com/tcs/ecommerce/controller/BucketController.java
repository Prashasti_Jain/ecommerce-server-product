package com.tcs.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;

import com.tcs.ecommerce.service.Impl.AmazonClient;

@CrossOrigin(origins = "http://13.233.184.83")
@RestController
@RequestMapping("/storage/")
public class BucketController {

	private AmazonClient amazonClient;

	@Autowired
	BucketController(AmazonClient amazonClient) {
		this.amazonClient = amazonClient;
	}

	@PostMapping(path = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@PreAuthorize("hasRole('VENDOR')")
	public String uploadFile(@RequestPart(value = "file") MultipartFile[] file) {
		String ss = (this.amazonClient.uploadFile(file));
		String st = "{\"imgId\":\"" + ss + "\"}";
		return st;
	}

}