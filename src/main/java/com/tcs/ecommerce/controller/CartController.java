package com.tcs.ecommerce.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.ecommerce.entity.Cart;
import com.tcs.ecommerce.entity.Wishlist;
import com.tcs.ecommerce.model.CartRequest;
import com.tcs.ecommerce.service.CartService;

@CrossOrigin(origins = "http://13.233.184.83")
@RestController
@Transactional
public class CartController {
	@Autowired
	private CartService service;

	@RequestMapping(method = RequestMethod.POST, value = "/addProductsToCart", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addToCart(@RequestBody CartRequest cart, @AuthenticationPrincipal String username)
			throws Exception {

		return new ResponseEntity<>(service.addProductsToCart(cart, username), HttpStatus.CREATED);
	}

	@GetMapping("/cartItems")
	public List<Cart> findAllCartItems() {
		return service.getCartItems();
	}

	@GetMapping("/cartItemsofUser")
	public List<Cart> getAllCartItems(@AuthenticationPrincipal String username) {
		List<Cart> cartList = (List<Cart>) service.getAllProductFromCart(username);
		return cartList;
	}

	@DeleteMapping("/deleteById/{id}")
	public String deleteProduct(@PathVariable Integer id, @AuthenticationPrincipal String username) {

		String abc = service.deleteProduct(id, username);
		return abc;
	}

	@PutMapping("/updateCart/{id}")
	public String changeQuantity(@PathVariable Integer id, @RequestBody int quantity,
			@AuthenticationPrincipal String username) {

		String abc = service.changeProductQty(id, quantity, username);
		return abc;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/addProductsToWishList", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> addToWishList(@RequestBody Integer productId, @AuthenticationPrincipal String username)
			throws Exception {

		return new ResponseEntity<>(service.addProductsToWishlist(productId, username), HttpStatus.CREATED);
	}

	@GetMapping("/wishListItemsofUser")
	public List<Wishlist> findAllWishListItems(@AuthenticationPrincipal String username) {
		return service.getWishListItems(username);
	}

	@DeleteMapping("/removeFromWishlist/{id}")
	public String removeProduct(@PathVariable Integer id, @AuthenticationPrincipal String username) {

		String abc = service.removeProductFromWishlist(id, username);
		return abc;
	}

	@GetMapping("/getAllwishListItems")
	public List<Wishlist> findWishListItems() {
		return service.getAllWishListItems();
	}
}
