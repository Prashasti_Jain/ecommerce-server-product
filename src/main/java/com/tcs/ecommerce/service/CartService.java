package com.tcs.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.tcs.ecommerce.converter.CartConverter;
import com.tcs.ecommerce.entity.Cart;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.entity.Wishlist;
import com.tcs.ecommerce.model.CartRequest;
import com.tcs.ecommerce.repository.CartRepository;
import com.tcs.ecommerce.repository.ProductRepository;
import com.tcs.ecommerce.repository.WishListRepository;
import com.tcs.ecommerce.utils.CommonConstants;

@Service
public class CartService {

	@Autowired
	private CartRepository cartRepo;
	@Autowired
	private ProductRepository productRepo;
	@Autowired
	private WishListRepository wishListRepo;

	// Get All Items from Cart
	public List<Cart> getCartItems() {
		return cartRepo.findAll();
	}

	// Add Product To cart
	public String addProductsToCart(CartRequest cart, @AuthenticationPrincipal String username) {
		Product product = productRepo.findByProductId(cart.getProductId());
		boolean alreadyExists = false;
		int pdQty = product.getProductQuantity();
		String result = "";

		List<Cart> cartList = cartRepo.findByUsername(username);
		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProduct().getProductId().equals(cart.getProductId())) {
				int qty = cartList.get(i).getQuantity();
				if (qty < pdQty) {
					cartList.get(i).setQuantity(qty + 1);
					cartList.get(i).setTotalPrice(cartList.get(i).getProduct().getProductPrice() * (qty + 1));
					cartRepo.save(cartList.get(i));
					result = CommonConstants.ADDED;
				} else {
					result = CommonConstants.NOTADDED;
				}
				alreadyExists = true;
			}
		}

		if (!alreadyExists) {
			if (pdQty > 0) {
				cartRepo.save(CartConverter.convertRegisterCartrModeltoEntity(cart, product, username));
				result = CommonConstants.ADDED;
			} else {
				result = CommonConstants.NOTADDED;
			}
		}

		return result;

	}

	public List<Cart> getAllProductFromCart(@AuthenticationPrincipal String username) {
		return cartRepo.findByUsername(username);
	}

	public String deleteProduct(Integer id, @AuthenticationPrincipal String username) {

		List<Cart> cartList = cartRepo.findByUsername(username);

		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProduct().getProductId().equals(id)) {
				cartRepo.deleteByCartId(cartList.get(i).getCartId());
			}
		}

		return "{\"value\":\"product removed !! " + id + "\"}";

	}

	public String changeProductQty(Integer pId, Integer quantity, @AuthenticationPrincipal String username) {

		String result = "";
		int pdQty = productRepo.findByProductId(pId).getProductQuantity();
		List<Cart> cartList = cartRepo.findByUsername(username);
		for (int i = 0; i < cartList.size(); i++) {
			if (cartList.get(i).getProduct().getProductId().equals(pId)) {
				if (quantity <= pdQty) {
					cartList.get(i).setQuantity(quantity);
					cartList.get(i).setTotalPrice(cartList.get(i).getProduct().getProductPrice() * quantity);
					cartRepo.save(cartList.get(i));
					result = CommonConstants.ADDED;
				} else {
					result = CommonConstants.NOTADDED;
				}

			}
		}

		return result;

	}

	public Wishlist addProductsToWishlist(Integer productId, String username) {

		Wishlist savedEntity = new Wishlist();
		List<Wishlist> wishList = wishListRepo.findAllByUsernameAndProductId(username, productId);

		if (wishList.isEmpty()) {
			savedEntity.setProductId(productId);
			savedEntity.setUsername(username);
			wishListRepo.save(savedEntity);
			return savedEntity;
		}

		return null;
	}

	public List<Wishlist> getWishListItems(@AuthenticationPrincipal String username) {
		return wishListRepo.findAllByUsername(username);
	}

	public String removeProductFromWishlist(Integer id, @AuthenticationPrincipal String username) {

		List<Wishlist> allWishlist = wishListRepo.findByUsername(username);

		for (int i = 0; i < allWishlist.size(); i++) {

			if (allWishlist.get(i).getProductId().equals(id)) {
				wishListRepo.deleteByWishListId(allWishlist.get(i).getWishListId());

				return "{\"value\":\"product removed !! " + id + "\"}";
			}
		}

		return "{\"value\":\"product not removed !! " + id + "\"}";

	}

	public List<Wishlist> getAllWishListItems() {

		return wishListRepo.findAll();
	}

}
