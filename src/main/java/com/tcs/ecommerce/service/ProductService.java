package com.tcs.ecommerce.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.tcs.ecommerce.converter.ProductConverter;
import com.tcs.ecommerce.entity.Brand;
import com.tcs.ecommerce.entity.Cart;
import com.tcs.ecommerce.entity.Category;
import com.tcs.ecommerce.entity.Image;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.model.ImageResponse;
import com.tcs.ecommerce.model.ProductRequest;
import com.tcs.ecommerce.model.ProductResponse;
import com.tcs.ecommerce.repository.BrandRepository;
import com.tcs.ecommerce.repository.CartRepository;
import com.tcs.ecommerce.repository.CategoryRepository;
import com.tcs.ecommerce.repository.ImageRepository;
import com.tcs.ecommerce.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	private ProductRepository repository;

	@Autowired
	private CategoryRepository repo;

	@Autowired
	private ImageRepository irepo;

	@Autowired
	private CartRepository cartRepo;

	@Autowired
	private BrandRepository brandRepo;

	public ProductResponse saveProduct(ProductRequest productReq, String username) {
		Category category = repo.findByCategoryId(productReq.getCategoryId());
		Brand brand = brandRepo.findByBrandId(productReq.getBrandId());

		Product prod = repository
				.save(ProductConverter.convertRegisterProductrModeltoEntity(productReq, category, brand));
		prod.setSellerName(username);
		List<Image> imgArr = saveMultipleImg(productReq, prod);
		ProductResponse prodResp = convProdToProdResp(prod, imgArr);
		return prodResp;
	}

	public List<Image> saveMultipleImg(ProductRequest prodReq, Product prod) {
		List<Image> allImgs = new ArrayList<Image>();
		String[] allUrls;
		if (prodReq.getImageIds().length() > 5) {
			allUrls = prodReq.getImageIds().split("; ");

			for (int i = 0; i < (allUrls.length); i++) {
				Image img1 = new Image();
				img1.setProduct(prod);
				img1.setImgURL(allUrls[i]);
				allImgs.add(irepo.save(img1));
			}
		}
		return allImgs;
	}

	public ProductResponse convProdToProdResp(Product product, List<Image> imgArr) {

		ProductResponse prodResponse = new ProductResponse();
		prodResponse.setProductId(product.getProductId());
		prodResponse.setProductName(product.getProductName());
		prodResponse.setProductDesc(product.getProductDesc());
		prodResponse.setProductPrice(product.getProductPrice());
		prodResponse.setProductStatus(product.getProductStatus());
		prodResponse.setProductQuantity(product.getProductQuantity());
		prodResponse.setSellerName(product.getSellerName());

		prodResponse.setCategoryId(product.getCategory().getCategoryId());

		prodResponse.setCategoryName(product.getCategory().getCategoryName());
		prodResponse.setColor(product.getColor());
		if (product.getBrand() != null) {
			prodResponse.setBrandId(product.getBrand().getBrandId());
			prodResponse.setBrandName(product.getBrand().getBrandName());
		}
		List<ImageResponse> imgResp = new ArrayList<ImageResponse>();
		for (int i = 0; i < imgArr.size(); i++) {
			ImageResponse tempImg = new ImageResponse();
			tempImg.setImageId(imgArr.get(i).getImageId());
			tempImg.setImageURL(imgArr.get(i).getImgURL());
			imgResp.add(tempImg);
		}
		prodResponse.setImage(imgResp);

		return prodResponse;
	}

	public List<Product> saveProducts(List<Product> products) {
		return repository.saveAll(products);
	}

	public List<ProductResponse> getProducts() {
		List<Product> source = (List<Product>) repository.findAll();
		List<ProductResponse> pResp = new ArrayList<ProductResponse>();
		for (int i = 0; i < source.size(); i++) {
			pResp.add(getProductById(source.get(i).getProductId()));
		}
		return pResp;
	}

	public List<ProductResponse> getProductsByName(String name) {

		List<Product> tempProd1 = repository.findAllByProductNameLike("%" + name + "%");
		List<Product> tempProds = new ArrayList<Product>();
		for (int i = 0; i < tempProd1.size(); i++) {
			if (tempProd1.get(i).getProductStatus().equalsIgnoreCase("Approved")) {
				tempProds.add(tempProd1.get(i));
			}
		}

		List<ProductResponse> tempProdResp = new ArrayList<ProductResponse>();
		for (int i = 0; i < tempProds.size(); i++) {
			List<Image> tempImgs = irepo.findByProduct(tempProds.get(i));
			tempProdResp.add(convProdToProdResp(tempProds.get(i), tempImgs));
		}

		return tempProdResp;
	}

	public List<ProductResponse> getProductsByProductStatus(String productStatus) {
		List<Product> tempProds = repository.findAllByProductStatus(productStatus);
		List<ProductResponse> tempProdResp = new ArrayList<ProductResponse>();
		for (int i = 0; i < tempProds.size(); i++) {
			List<Image> tempImgs = irepo.findAllByProduct(tempProds.get(i));
			tempProdResp.add(convProdToProdResp(tempProds.get(i), tempImgs));
		}
		return tempProdResp;
	}

	public ProductResponse getProductById(Integer id) {

		List<Image> imgArray = irepo.findAllByProduct(repository.findByProductId(id));
		return convProdToProdResp(repository.findByProductId(id), imgArray);

	}

	public List<ProductResponse> getAllProductsById(int[] pIds) {
		List<ProductResponse> allProds = new ArrayList<ProductResponse>();
		for (int i = 0; i < pIds.length; i++) {
			allProds.add(getProductById(pIds[i]));
		}
		return allProds;
	}

	public String deleteProduct(int id, @AuthenticationPrincipal String username) {

		Product prod = repository.findByProductId(id);
		if (prod.getSellerName().equalsIgnoreCase(username)) {
			prod.setProductStatus("Deleted");
			prod.setProductQuantity(0);
			repository.save(prod);
			List<Cart> cartList = cartRepo.findAllByProduct(repository.findByProductId(id));
			for (int i = 0; i < cartList.size(); i++) {
				cartList.get(i).setQuantity(0);
				cartRepo.save(cartList.get(i));
			}

			return "product removed !! " + id;

		} else {
			return "You are not Authorized to remove This Product with id=" + id;
		}

	}

	public String updateProductStatus(int pId, int stat) {
		Product existingProduct = repository.findByProductId(pId);
		if (existingProduct.getProductStatus().equalsIgnoreCase("pending")) {
			if (stat == 1) {
				existingProduct.setProductStatus("Approved");
			} else if (stat == -1) {
				existingProduct.setProductStatus("Denied");
			}
		}
		repository.save(existingProduct);
		return "{\"Value\":\"" + "Status Updated" + "\"}";
	}

	public Category saveCategory(Category category) {
		return repo.save(category);
	}

	public List<ProductResponse> getProductsByCreatedBy(String createdBy) {
		List<Product> tempProds = repository.findAllBySellerName(createdBy);
		List<ProductResponse> tempProdResp = new ArrayList<ProductResponse>();
		for (int i = 0; i < tempProds.size(); i++) {
			List<Image> tempImgs = irepo.findAllByProduct(tempProds.get(i));
			tempProdResp.add(convProdToProdResp(tempProds.get(i), tempImgs));
		}
		return tempProdResp;
	}

	public String deleteImage(int[] deleteList) {
		for (int i = 0; i < deleteList.length; i++) {
			irepo.deleteByImageId(deleteList[i]);
		}
		return "{\"Value\":\"" + deleteList.length + " images Deleted\"}";
	}

	public String updateProduct(ProductRequest productReq, int id) {

		Product existingProduct = repository.findByProductId(id);
		Category category = repo.findByCategoryId(productReq.getCategoryId());
		Brand brand = brandRepo.findByBrandId(productReq.getBrandId());
		if (existingProduct.getCategory().getCategoryName().equals(category.getCategoryName())
				&& existingProduct.getBrand().getBrandName().equals(brand.getBrandName())
				&& existingProduct.getProductDesc().equals(productReq.getProductDesc())
				&& existingProduct.getProductName().equals(productReq.getProductName())) {

		} else {
			existingProduct.setProductStatus("Pending");
		}
		existingProduct.setBrand(brand);
		existingProduct.setCategory(category);
		existingProduct.setColor(productReq.getColor());
		existingProduct.setProductDesc(productReq.getProductDesc());
		existingProduct.setProductName(productReq.getProductName());
		existingProduct.setProductPrice(productReq.getProductPrice());
		existingProduct.setProductQuantity(existingProduct.getProductQuantity() + productReq.getProductQuantity());
		existingProduct.setProductDesc(productReq.getProductDesc());

		repository.save(existingProduct);
		if (productReq.getImageIds().length() > 5)
			saveMultipleImg(productReq, existingProduct);
		return "{\"Value\":\"" + id + "\"}";
	}

	public Category addCategory(String category) {
		Category cat = repo.findByCategoryName2(category.toLowerCase());
		if (cat != null) {

			return cat;
		} else {
			Category saveCat = new Category();
			saveCat.setCategoryName(category);
			return repo.save(saveCat);
		}
	}

	public List<Category> getCategories() {
		return repo.findAll();
	}

	public Brand addBrand(String brand) {
		Brand brandObj = brandRepo.findByBrandName2(brand.toLowerCase());

		if (brandObj != null) {
			return brandObj;
		} else {
			Brand saveBrand = new Brand();
			saveBrand.setBrandName(brand);
			return brandRepo.save(saveBrand);
		}

	}

	public List<Brand> getBrands() {
		return brandRepo.findAll();
	}

}
