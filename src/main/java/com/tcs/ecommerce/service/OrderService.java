package com.tcs.ecommerce.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.tcs.ecommerce.entity.Cart;
import com.tcs.ecommerce.entity.Order;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.repository.CartRepository;
import com.tcs.ecommerce.repository.OrderRepository;
import com.tcs.ecommerce.repository.ProductRepository;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private CartService cartService;
	@Autowired
	private CartRepository cartRepo;
	@Autowired
	private ProductRepository productRepo;

	public String saveOrders(List<Order> orders, @AuthenticationPrincipal String username) {
		String result = "{\"value\":[";
		for (int i = 0; i < orders.size(); i++) {
			orders.get(i).setDate(new Date());
			int Pid = orders.get(i).getProductId();

			Product prod = productRepo.findByProductId(Pid);
			if (prod.getProductQuantity() >= orders.get(i).getQuantity()) {

				orders.get(i).setUsername(username);
				prod.setProductQuantity(prod.getProductQuantity() - orders.get(i).getQuantity());
				productRepo.save(prod);
				List<Cart> cartList = cartRepo.findAllByProduct(prod);

				if (cartList != null) {
					for (int j = 0; j < cartList.size(); j++) {
						if (cartList.get(j).getUsername().equals(username)) {

						} else {

							if (cartList.get(j).getQuantity() > prod.getProductQuantity()) {
								cartList.get(j).setQuantity(prod.getProductQuantity());
								cartList.get(j).setTotalPrice(prod.getProductPrice() * cartList.get(j).getQuantity());
								cartRepo.save(cartList.get(j));
							}
						}
					}
				}
				orderRepository.save(orders.get(i));
				result += "{\"" + orders.get(i).getProductId() + "\":\"Success\"},";
			} else {
				result += "{\"" + orders.get(i).getProductId() + "\":\"Unsuccess\"},";
			}
		}
		for (int i = 0; i < orders.size(); i++) {
			cartService.deleteProduct(orders.get(i).getProductId(), username);
		}

		return result + "{}]}";
	}

	public List<Order> getOrders(@AuthenticationPrincipal String username) {

		return orderRepository.findAllByUsername(username);
	}

	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

}
