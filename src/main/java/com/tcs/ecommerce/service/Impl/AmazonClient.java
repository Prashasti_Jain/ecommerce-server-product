package com.tcs.ecommerce.service.Impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;

@Transactional
@Service
@Component
public class AmazonClient {

	private AmazonS3 s3client;

	@Value("${endpointUrl}")
	private String endpointUrl;
	@Value("${bucketname}")
	private String bucketName;
	@Value("${accessKey}")
	private String accessKey;
	@Value("${secretKey}")
	private String secretKey;

	@PostConstruct
	private void initializeAmazon() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		this.s3client = new AmazonS3Client(credentials);
	}

	public String uploadFile(MultipartFile[] file) {

		String uuids = "";

		for (int i = 0; i < file.length; i++) {

			String uuid = UUID.randomUUID().toString();

			try {

				ByteArrayInputStream stream = new ByteArrayInputStream(file[i].getBytes());

				ObjectMetadata meta = new ObjectMetadata();

				meta.setContentLength(file[i].getSize());

				meta.setContentType(file[i].getContentType());

				s3client.putObject(bucketName, uuid, stream, meta);

			} catch (Exception e) {

				e.printStackTrace();

			}

			uuids = uuids + uuid + "; ";
		}
		return uuids;

	}

	public String uploadFileTest(File file) {

		String uuid = UUID.randomUUID().toString();
		try {

			AWSCredentials credentials = new BasicAWSCredentials("AKIAXZHHVZT5JJYOP3YX",
					"NQijMiZt9ixvOt/UwL45+AoQ+F24UoG1j8dqhrvG");

			AmazonS3Client s3ClientTest = new AmazonS3Client(credentials);

			s3ClientTest.putObject("megamart-bucket", uuid, file);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return uuid;
	}

	public static void main(String arg[]) {
		String path = "C:\\Users\\1816255\\Desktop\\Usecase\\wire frame\\Login_Page.png";
		File file = new File(path);

	}

}
