package com.tcs.ecommerce.service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.tcs.ecommerce.converter.ReviewConverter;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.entity.ProductReview;
import com.tcs.ecommerce.exception.EcommerceRestException;
import com.tcs.ecommerce.model.ReviewRequest;
import com.tcs.ecommerce.model.ReviewResponse;
import com.tcs.ecommerce.repository.ProductRepository;
import com.tcs.ecommerce.repository.ReviewRepository;

@Service
public class ProductReviewService {

	@Autowired
	private ReviewRepository reviewRepo;
	@Autowired
	private ProductRepository productRepo;

	public ReviewResponse addProductReview(ReviewRequest req, @AuthenticationPrincipal String username)
			throws Exception {

		Product product = productRepo.findByProductId(req.getProductId());

		if (product == null) {
			throw new EcommerceRestException("Product not found", HttpStatus.BAD_REQUEST);
		}
		ReviewResponse review = new ReviewResponse();
		List<ProductReview> temp = reviewRepo.findAllByProductAndUsername(product, username);
		if (temp == null || temp.size() == 0) {
			ProductReview pReview = ReviewConverter.convertProductRequestModelToEntity(req, product, username);
			ProductReview savedEntity = reviewRepo.save(pReview);
			review.setComment(savedEntity.getComment());
			review.setRating(savedEntity.getRating());
			review.setHeading(savedEntity.getHeading());
			review.setDate(savedEntity.getDate());
		} else {
			temp.get(0).setComment(req.getComment());
			temp.get(0).setRating(req.getRating());
			temp.get(0).setHeading(req.getHeading());
			temp.get(0).setDate(new Date());
			reviewRepo.save(temp.get(0));
			review.setComment(temp.get(0).getComment());
			review.setRating(temp.get(0).getRating());
			review.setHeading(temp.get(0).getHeading());
			review.setDate(temp.get(0).getDate());
		}
		return review;
	}

	public String getAverageRating(int pid) {
		double avg = 0, total = 0, cnt = 0;
		Product tempProd = productRepo.findByProductId(pid);
		List<ProductReview> prodRev = reviewRepo.findAllByProduct(tempProd);

		for (int i = 0; i < prodRev.size(); i++) {
			total += (double) prodRev.get(i).getRating();
			cnt += 1;
		}
		if (cnt != 0)
			avg = total / cnt;
		DecimalFormat df = new DecimalFormat("#.#");

		return "{\"Value\":\"" + df.format(avg) + "\",\"count\":\"" + (int) cnt + "\"}";
	}

	public List<ProductReview> findAllbyProductId(int pid) {
		return reviewRepo.findAllByProduct(productRepo.findByProductId(pid));
	}

	public List<ProductReview> findAllbyUser(@AuthenticationPrincipal String username) {
		return reviewRepo.findAllByUsername(username);
	}

	public ReviewResponse getAlreadyRated(int pid, @AuthenticationPrincipal String username) {
		ProductReview temp2 = reviewRepo.findByProductAndUsername(productRepo.findByProductId(pid), username);
		ReviewResponse tempRR = new ReviewResponse();
		if (temp2 != null) {
			tempRR.setRating(temp2.getRating());
			tempRR.setComment(temp2.getComment());
			tempRR.setHeading(temp2.getHeading());

		}
		return tempRR;
	}

	public String removeReview(Integer pid, @AuthenticationPrincipal String username) {
		ProductReview temp2 = reviewRepo.findByProductAndUsername(productRepo.findByProductId(pid), username);
		reviewRepo.deleteByReviewId(temp2.getReviewId());

		return "{\"Value\":\"Deleted\"}";
	}
}
