package com.tcs.ecommerce.converter;


import java.util.Date;

import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.entity.ProductReview;
import com.tcs.ecommerce.model.ReviewRequest;

public class ReviewConverter {

	private ReviewConverter() {
		
	}
	
	public static ProductReview convertProductRequestModelToEntity(ReviewRequest review,Product product,String username) {
		ProductReview pReview = new ProductReview();
		pReview.setComment(review.getComment());
		pReview.setRating(review.getRating());
		pReview.setProduct(product);
		pReview.setUsername(username);
		pReview.setHeading(review.getHeading());
		pReview.setDate(new Date());
		return pReview;
		
	}
}
