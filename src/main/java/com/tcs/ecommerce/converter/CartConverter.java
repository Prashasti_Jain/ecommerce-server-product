package com.tcs.ecommerce.converter;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import com.tcs.ecommerce.entity.Cart;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.model.CartRequest;

public class CartConverter {

	private CartConverter() {

	}

	public static Cart convertRegisterCartrModeltoEntity(CartRequest cart, Product product,
			@AuthenticationPrincipal String username) {
		Cart entityObj = new Cart();
		entityObj.setUsername(cart.getUsername());
		entityObj.setProduct(product);
		entityObj.setQuantity(cart.getQuantity());
		entityObj.setUsername(username);
		entityObj.setTotalPrice(cart.getTotalPrice());

		return entityObj;
	}
	
	

}
