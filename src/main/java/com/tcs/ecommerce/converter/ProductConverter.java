package com.tcs.ecommerce.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tcs.ecommerce.entity.Brand;
import com.tcs.ecommerce.entity.Category;
import com.tcs.ecommerce.entity.Image;
import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.model.ImageResponse;
import com.tcs.ecommerce.model.ProductRequest;
import com.tcs.ecommerce.model.ProductResponse;

public final class ProductConverter {

	private ProductConverter() {

	}

	public static Product convertRegisterProductrModeltoEntity(ProductRequest product, Category category, Brand brand) {

		Product entityObj = new Product();

		entityObj.setProductName(product.getProductName());
		entityObj.setProductDesc(product.getProductDesc());
		entityObj.setProductPrice(product.getProductPrice());
		entityObj.setCategory(category);
		entityObj.setBrand(brand);
		entityObj.setColor(product.getColor());

		entityObj.setProductStatus("Pending");
		entityObj.setProductQuantity(product.getProductQuantity());
		return entityObj;

	}

	public static List<ProductResponse> mapProduct(List<Product> source, Map<Integer, List<Image>> imageMap) {
		List<ProductResponse> productList = new ArrayList<>();
		if (source != null) {
			for (Product entity : source) {
				ProductResponse p = new ProductResponse();

				p.setProductId(entity.getProductId());
				p.setProductName(entity.getProductName());
				p.setProductDesc(entity.getProductDesc());
				p.setProductPrice(entity.getProductPrice());
				p.setProductQuantity(entity.getProductQuantity());
				p.setColor(entity.getColor());
				if (entity.getCategory() != null) {
					p.setCategoryId(entity.getCategory().getCategoryId());
					p.setCategoryName(entity.getCategory().getCategoryName());
				}
				List<Image> images = imageMap.get(entity.getProductId());
				if (images != null) {
					List<ImageResponse> prdImages = new ArrayList<>();
					for (Image img : images) {
						ImageResponse image = new ImageResponse();
						image.setImageId(img.getImageId());
						image.setImageURL(img.getImgURL());
						prdImages.add(image);
					}
					p.setImage(prdImages);
				}
				productList.add(p);
			}
		}
		return productList;
	}

}