package com.tcs.ecommerce.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name="seqCatg", initialValue=8, allocationSize=1)
@Table(name = "CATEGORY",schema="product")
public class Category {

	@Id

	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqCatg")
	@Column(name = "category_Id")
    private Integer categoryId;
    private String categoryName;
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
    
    
    
}
