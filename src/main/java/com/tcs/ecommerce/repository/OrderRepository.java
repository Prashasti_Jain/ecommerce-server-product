package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.ecommerce.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

	List<Order> findAllByUsername(String username);

}
