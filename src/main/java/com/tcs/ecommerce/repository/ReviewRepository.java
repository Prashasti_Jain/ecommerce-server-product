package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.entity.ProductReview;

public interface ReviewRepository extends JpaRepository<ProductReview, Integer>{


	ProductReview save(String review);

	ProductReview findByreviewIdAndProduct(Integer id, Product product);

	List<ProductReview> findAllByProduct(Product product);

	List<ProductReview> findAllByUsername(String username);

	List<ProductReview> findAllByProductAndUsername(Product product, String username);

	ProductReview findByProductAndUsername(Product findByProductId, String username);

	void deleteByReviewId(Integer reviewId);


}
