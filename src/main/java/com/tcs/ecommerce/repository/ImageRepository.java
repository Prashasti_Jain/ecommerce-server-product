package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tcs.ecommerce.entity.Image;
import com.tcs.ecommerce.entity.Product;

public interface ImageRepository extends JpaRepository<Image, Integer> {

	Image findByImageId(Integer imageId);

	@Query("select img from Image img where img.product.productId in :productIds")
	List<Image> findByProductIdIn(@Param("productIds") List<Integer> productIds);

	List<Image> findByProductIn(List<Product> source);

	List<Image> findAllByProduct(Product id);

	void deleteAllByProduct(Product id);

	List<Image> findAllByProductIn(List<Integer> productIds);

	List<Image> findByProduct(Product product);

	void deleteByImageId(int i);

}
