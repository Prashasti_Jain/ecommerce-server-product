package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcs.ecommerce.entity.Wishlist;

@Repository
public interface WishListRepository extends JpaRepository<Wishlist, Integer> {

	public List<Wishlist> findByUsername(String username);

	public void deleteByWishListId(Integer wishlistId);

	public List<Wishlist> findAllByProductId(Integer productId);

	public List<Wishlist> findAllByUsername(String username);

	public List<Wishlist> findAllByUsernameAndProductId(String username, Integer productId);

	public Wishlist findByWishListId(Integer id);

}
