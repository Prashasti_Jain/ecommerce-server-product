package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.ecommerce.entity.Product;
import com.tcs.ecommerce.model.ProductRequest;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	Product findByProductId(Integer id);

	Product findByProductName(String name);

	void deleteByProductId(int id);

	Product save(ProductRequest product);

	List<Product> findAllByProductName(String productName);

	List<Product> findAllByProductStatus(String productStatus);

	Product findByProductId(String productId);

	List<Product> findAllBySellerName(String createdBy);

	List<Product> findAllByProductNameLike(String name);

}
