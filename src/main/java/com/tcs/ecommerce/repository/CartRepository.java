package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.ecommerce.entity.Cart;
import com.tcs.ecommerce.entity.Product;

public interface CartRepository extends JpaRepository<Cart, Integer> {

	public Cart findBycartId(Integer cartId);

	public List<Cart> findByUsername(String username);

	public void deleteByCartId(Integer cartId);

	public List<Cart> findAllByProduct(Product productId);

}
