package com.tcs.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tcs.ecommerce.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

	Category findByCategoryId(Integer categoryId);

	Category findByCategoryName(String category);

	@Query("select c from Category c where LOWER(c.categoryName)= :categoryName")
	Category findByCategoryName2(String categoryName);

}
