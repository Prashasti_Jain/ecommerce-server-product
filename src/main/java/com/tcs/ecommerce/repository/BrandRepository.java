package com.tcs.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tcs.ecommerce.entity.Brand;

public interface BrandRepository extends JpaRepository<Brand, Integer> {

	Brand findByBrandName(String brand);

	Brand findByBrandId(Integer brandId);

	@Query("select b from Brand b where LOWER(b.brandName)= :brandname")
	Brand findByBrandName2(String brandname);

}
